var express = require('express')
var router = express.Router();
var db = require('./../models');
var XLSX = require('xlsx');
const { uuid } = require('uuidv4');
import { Op } from 'sequelize';

// import badgeCreator from '../core/pdf';
import badgeCreator from '../core/badge';
import ListPdf from '../core/listpdf';
import ListExcel from '../core/listexcel';
import QRCode from 'qrcode';
import badgeCreatorZip from '../core/create_badge';
import path from 'path';

router.get('/', function(req, res) {
    req.session.active = "student"
    var whereclose = {};
    if (req.session.user.role != "admin" && req.session.user.role != "superadmin") {
        whereclose = {
            category: req.session.user.category
        }
    }
    db.Student.findAll({
        where: whereclose,
        include: [{ model: db.Program, required: false, as: "presences" }],
        order: [
            ['name', 'ASC'],
            ['lastname', 'ASC']
        ]
    }).then((data) => {
        res.render('admin/student/index', { students: data });
    })

});
router.get('/add', function(req, res) {
    req.session.active = "student"
    res.render('admin/student/add');
});

router.post('/add', function(req, res) {
    var imagefile = req.files;
    var datajson = req.body;
    if (imagefile != null) {

        var imagename = uuid()+'_'+imagefile.file.name
        var uploadPath = __dirname + '/../../public/assets/profile/students/' + imagename;
        imagefile.file.mv(uploadPath, function(err) {
            if (err) return res.status(500).send(err);
            delete datajson['file']
            datajson['image'] = imagename;
            // datajson['role'] = 'superadmin';
            db.Student.create(datajson).then((rep) => {
                res.redirect('/admin/student');
            }).catch((error) => {
                return res.status(500).send(error);
            });

        });
    } else {
        db.Student.create(datajson).then(() => {
            res.redirect('/admin/student');
        }).catch((error) => {
            console.log('Error');
            res.send(error).status(500);
        });
    }


});
router.get('/edit/:id', function(req, res) {
    req.session.active = "student"
    db.Student.findOne({ where: { id: req.params.id } }).then((data) => {
        console.log(data)
        if (data === null) {
            res.redirect('/admin/student')
        } else {
            res.render('admin/student/edit', { student: data });
        }
    }).catch((error) => {
        res.status(500).send(error);
    });
});

router.post('/edit', function(req, res) {
    var imagefile = req.files;
    var datajson = req.body;
    if (imagefile != null) {
        var imagename = uuid()+'_'+imagefile.file.name
        var uploadPath = __dirname + '/../../public/assets/profile/students/' + imagename;
        imagefile.file.mv(uploadPath, function(err) {
            if (err) return res.status(500).send(err);
            var datajson = req.body;
            delete datajson['file']
            datajson['image'] = imagename;
            var id = datajson.id;
            delete datajson['id']
            db.Student.update(datajson, { where: { id: id } }).then(() => {
                console.log('Updated')
                res.redirect('/admin/student');
            }).catch((error) => {
                console.log('Error');
                res.send(error).status(500);
            })

        });
    } else {
        var id = datajson.id;
        delete datajson['id'];
        db.Student.update(datajson, { where: { id: id } }).then(() => {
            console.log('Updated')
            res.redirect('/admin/student');
        }).catch((error) => {
            console.log('Error');
            res.send(error).status(500);
        });
    }
});

router.post('/excel', function(req, res) {
    var excelfile = req.files;
    var extension = excelfile.file.name.split('.').slice(-1)[0];
    var newname = uuid() + '.' + extension;
    var uploadPath = __dirname + '/../../public/assets/profile/students/' + newname;
    excelfile.file.mv(uploadPath, function(err) {
        var workbook = XLSX.readFile(uploadPath);
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];
        var jsondata = XLSX.utils.sheet_to_json(worksheet)
        db.Student.bulkCreate(jsondata).then((resp) => {
            console.log(jsondata);
            res.redirect('/admin/student');
        })
    });
});

router.post('/delete', function(req, res) {
    db.Student.destroy({
        where: { id: req.body.id }
    }).then((rep) => {
        res.redirect('/admin/student');
    }).catch((error) => {
        return res.status(500).send(error);
    });
});

router.get('/search', function(req, res) {
    res.redirect('/admin/student')
})

router.get('/badge/:id', function(req, res) {
    req.session.active = "student";
    db.Student.findOne({ where: { id: req.params.id }, raw: true, nest: true }).then((std) => {
        var badge_name = std.id + '_' + std.lastname.split(' ').join('_') + '_' + std.name.split(' ').join('_')+uuid();
        var qrfile = path.join(__dirname ,'/../../public/assets/badges/'+badge_name+'_qrcode.png');
        QRCode.toFile(qrfile, std.matricule, {
            color: {
                // dark: "#E04343",
                light: "#fff"
            },
            version: 5
        }, function(error) {
            if (error) res.status(500).send(error);
            var date = new Date();
            var year = date.getFullYear();
            badgeCreator(res, std, qrfile, function(filepath) {

            }, true,"31-12-"+year,true);
        });
    }).catch((error) => {
        res.status(500).send(error);
    });

});

router.post('/export', function(req, res) {
    req.session.active = "student";
    var whereclause = req.body
    var export_type = whereclause['type'];
    delete whereclause['type']
    var isSign = false;
    if ('presence' in whereclause) {
        isSign = true;
        delete whereclause['presence']
    }
    if (whereclause.category == "tous") delete whereclause['category']
    if (whereclause.time == "tous") delete whereclause['time']
    db.Student.findAll({ where: whereclause, raw: true, nest: true }).then((stds) => {
        if (export_type === "pdf") {
            ListPdf(stds, res, isSign);
        } else {
            ListExcel(stds, res)
        }
    }).catch((error) => {
        res.status(500).send(error);
    });

});

router.post('/badges', function(req, res) {
    req.session.active = "student";
    var whereclause = req.body
    delete whereclause['type']
    var category = whereclause.category;
    if (whereclause.category == "tous") delete whereclause['category']
    if (whereclause.time == "tous") delete whereclause['time']
    var expiration;
    if (whereclause.expiration == ""){
        var today = new Date()
        expiration = '31-12-'+today.getFullYear()
    }
    else{
        var date = new Date(whereclause.expiration);
        if(date){
            var month = date.getMonth()+1
            expiration = date.getDate()+'-'+month+'-'+date.getFullYear()
        }
        else{
            var today = new Date()
            expiration = '31-12-'+today.getFullYear()
        }
    }
    delete whereclause['expiration']
    db.Student.findAll({ where: whereclause, raw: true, nest: true }).then((stds) => {
       badgeCreatorZip(res,expiration,stds,0,[],function(data){
            res.zip(data,'badges_'+category+'.zip');
       })
    })
    .catch((error) => {
        res.status(500).send(error);
    });

});


router.post('/search', function(req, res) {
    var query = req.body.search;
    db.Student.findAll({
        where: {
            [Op.or]: {
                name: {
                    [Op.like]: "%" + query + "%"
                },
                lastname: {
                    [Op.like]: "%" + query + "%"
                },
                matricule: {
                    [Op.like]: "%" + query + "%"
                }
            }
        },
        include: [{ model: db.Program, required: false, as: "presents" }],
        order: [
            ['name', 'ASC'],
            ['lastname', 'ASC']
        ]
    }).then((data) => {
        res.render('admin/student/index', { students: data });;
    }).catch((error) => {
        return res.status(500).send(error);
    });
});
export default router;