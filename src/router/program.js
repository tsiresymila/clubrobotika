var express = require("express")
var router = express.Router()
const crypto = require("crypto")
var path = require("path")
import db from "../models"
const { Op } = require("sequelize")
var fs = require("fs")
import { streamToBuffer } from "@jorgeferrero/stream-to-buffer"

router.get("/", function(req, res) {
    req.session.active = "program"
    var whereclose = {}
    if (req.session.user.role != "superadmin") {
        whereclose = {
            category: req.session.user.category
        }
    }
    db.Program.findAll({
            where: whereclose,
            include: [{
                    model: db.File,
                    required: false,
                    as: "files"
                },
                {
                    model: db.Exercice,
                    required: false,
                    as: "exercices"
                },
                { model: db.Coach, required: false, as: "coachs" }
            ],
            order: [
                ["numero", "ASC"]
            ]
        })
        .then(function(data) {
            db.File.findAll({ raw: true, nest: true })
                .then(function(files) {
                    db.Student.findAll({ raw: true, nest: true, where: whereclose })
                        .then(function(students) {
                            res.render("admin/program/index", {
                                programs: data,
                                files: files,
                                students: students
                            })
                        })
                        .catch(function(error) {
                            res.status(500).send(error)
                        })
                })
                .catch(function(error) {
                    res.status(500).send(error)
                })
        })
        .catch(function(error) {
            res.status(500).send(error)
        })
})

router.get("/view/:id", function(req, res) {
    db.File.findOne({ where: { id: req.params.id } }).then(function(file) {
        if (file != null) {
            var filepath = __dirname + "/../../public/documents/" + file.name
            if (file.type.split("/").slice(-1)[0] === "pdf") {
                var stream = fs.createReadStream(filepath, "base64")
                streamToBuffer(stream).then((data) => {
                    res.render("admin/program/pdf", { file: file, stream: data })
                })
            } else if (file.type.split("/")[0] === "video") {
                var stream = fs.createReadStream(filepath, "base64")
                streamToBuffer(stream).then((data) => {
                    res.render("admin/program/video", {
                        file: file,
                        stream: "data:" + file.type + ";base64," + data
                    })
                })
            } else {
                res.redirect("/admin/program")
            }
        } else {
            res.redirect("/admin/program")
        }
    })
})

router.get("/download/:id", function(req, res) {
    db.File.findOne({ where: { id: req.params.id } }).then(function(file) {
        if (file != null) {
            var filepath = __dirname + "/../../public/documents/" + file.name
            var stream = fs.createReadStream(filepath)
            res.setHeader('Content-Length', file.size);
            res.setHeader('Content-Type', file.type);
            res.setHeader('Content-Disposition', 'attachment; filename=' + file.name);
            stream.pipe(res);
        } else {
            res.redirect("/admin/program")
        }
    })
})

router.get("/file/remove/:id", function(req, res) {
    db.PFile.destroy({ where: { FileId: req.params.id } }).then(
        function(indice) {
            res.redirect("/admin/program")
        }
    )
})
router.post("/add", function(req, res) {
    var jsondata = req.body
    var imagefile = req.files;
    if (imagefile != null) {
        var uploadPath = __dirname + '/../../public/assets/programs/' + imagefile.file.name;
        imagefile.file.mv(uploadPath, function(err) {
            if (err) return res.status(500).send(err);
            jsondata['image'] = imagefile.file.name;
            db.Program.create(jsondata)
                .then(function(indice) {
                    db.Notification.create({
                        title: "Nouveau programme ajouté",
                        subtitle: jsondata.title,
                        category: jsondata.category,
                        description: ""
                    }).then(() => {
                        res.redirect("/admin/program")
                    })

                })
                .catch(function(err) {
                    res.status(500).send(err)
                })

        });
    } else {
        db.Program.create(jsondata)
            .then(function(indice) {
                db.Notification.create({
                    title: "Nouveau programme ajouté",
                    subtitle: jsondata.title,
                    category: jsondata.category,
                    description: ""
                }).then(() => {
                    res.redirect("/admin/program")
                })
            })
            .catch(function(err) {
                res.status(500).send(err)
            })
    }
})

router.post("/delete", function(req, res) {
    var jsondata = req.body
    db.Program.destroy({ where: { id: jsondata.id } })
        .then(function(indice) {
            res.redirect("/admin/program")
        })
        .catch(function(err) {
            res.status(500).send(err)
        })
})

router.post("/file/add", function(req, res) {
    var jsondata = req.body
    db.Program.findByPk(jsondata.id).then((program) => {
        if (typeof jsondata.files === typeof []) {
            var increment = 0
            jsondata.files.forEach(function(id, index, array) {
                increment++
                db.PFile.findOne({ where: { FileId: id } }).then(function(pfile) {
                    if (pfile) {
                        pfile.update({ programid: program.id, FileId: id, type: jsondata.type }).then(function(i) {
                            if (increment === array.length) {
                                res.redirect("/admin/program")
                            }
                        })
                    } else {
                        db.PFile.create({ programid: program.id, FileId: id, type: jsondata.type }).then(
                            function(indice) {
                                if (increment === array.length) {
                                    res.redirect("/admin/program")
                                }
                            }
                        )
                    }
                })
            })
        } else {
            db.File.findByPk(jsondata.files).then(function(file) {
                db.PFile.findOne({ where: { FileId: file.id } }).then(function(pfile) {
                    if (pfile) {
                        pfile.update({ programid: program.id, FileId: file.id, type: jsondata.type }).then(function(i) {
                            res.redirect("/admin/program")
                        })
                    } else {
                        db.PFile.create({ programid: program.id, FileId: file.id, type: jsondata.type }).then(
                            function(indice) {
                                res.redirect("/admin/program")
                            }
                        )
                    }
                })

            })
        }
    })
})

router.post("/update", function(req, res) {
    var jsondata = req.body
    var id = jsondata.id
    delete jsondata["id"]
    var imagefile = req.files;
    if (imagefile != null) {
        var uploadPath = __dirname + '/../../public/assets/programs/' + imagefile.file.name;
        imagefile.file.mv(uploadPath, function(err) {
            if (err) return res.status(500).send(err);
            jsondata['image'] = imagefile.file.name;
            db.Program.update(jsondata, { where: { id: id } })
                .then(function(indice) {
                    res.redirect("/admin/program")
                })
                .catch(function(err) {
                    res.status(500).send(err)
                })
        });
    } else {
        db.Program.update(jsondata, { where: { id: id } })
            .then(function(indice) {
                res.redirect("/admin/program")
            })
            .catch(function(err) {
                res.status(500).send(err)
            })
    }
})

router.post("/add/exercice", function(req, res) {
    var jsondata = req.body
    var imagefile = req.files;

    var event = {
        title: "Exercice à rendre",
        description: jsondata.description,
        date: jsondata.closeDate,
        category: jsondata.category
    }
    if (imagefile != null) {
        var uploadPath = __dirname + '/../../public/assets/programs/' + imagefile.file.name;
        imagefile.file.mv(uploadPath, function(err) {
            if (err) return res.status(500).send(err);
            jsondata['file'] = imagefile.file.name;
            db.Exercice.create(jsondata)
                .then(function(indice) {
                    db.Event.create(event).then(function(ok) {
                        db.Notification.create({
                            title: "Nouveau exercice ajouté",
                            subtitle: jsondata.title,
                            category: jsondata.category,
                            description: jsondata.description
                        }).then(() => {
                            res.redirect("/admin/program")
                        })
                    }).catch(function(err) {
                        res.status(500).send(err)
                    })
                })
                .catch(function(err) {
                    res.status(500).send(err)
                })
        });
    } else {
        db.Exercice.create(jsondata)
            .then(function(indice) {
                db.Event.create(event).then(function(ok) {
                    db.Notification.create({
                        title: "Nouveau exercice ajouté",
                        subtitle: jsondata.title,
                        category: jsondata.category,
                        description: jsondata.description
                    }).then(() => {
                        res.redirect("/admin/program")
                    })
                }).catch(function(err) {
                    res.status(500).send(err)
                })
            })
            .catch(function(err) {
                res.status(500).send(err)
            })
    }

})

router.post("/present", function(req, res) {
    var jsondata = req.body
    db.PStudent.findOne({
            where: {
                programid: jsondata["programid"],
                studentid: jsondata["studentid"]
            }
        })
        .then((sp) => {
            if (sp != null) {
                sp.update(jsondata).then(() => {
                    res.redirect("/admin/program")
                })
            } else {
                db.PStudent.create(jsondata)
                    .then((sp) => {
                        res.redirect("/admin/program")
                    })
                    .catch((err) => {
                        res.status(500).send(err)
                    })
            }
        })
        .catch((err) => {
            res.status(500).send(err)
        })
})

router.post("/state", function(req, res) {
    var jsondata = req.body
    db.Cprogram.findOne({
            where: {
                programid: jsondata["programid"],
                coachid: jsondata["coachid"]
            }
        })
        .then((cp) => {
            if (cp != null) {
                cp.destroy().then(() => {
                    res.send("OK")
                })
            } else {
                db.Cprogram.create(jsondata)
                    .then((sp) => {
                        res.send("OK")
                    })
                    .catch((err) => {
                        res.status(500).send(err)
                    })
            }
        })
        .catch((err) => {
            res.status(500).send(err)
        })
})

export default router