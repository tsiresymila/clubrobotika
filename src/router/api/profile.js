import express from 'express';



let router = express.Router();
router.get('/profile', (req, res) => {
    console.log(req.user)
    res.status(200).json({ data: req.user });
})

export default router;