import express from 'express';
import crypto from 'crypto';
import db from '../../models';
import { Op } from "sequelize";
import generateToken from '../../core/generate_token';


let router = express.Router();
router.post('/login', (req, res) => {
    var data = req.body;
    db.Student.findOne({
        raw: true,
        nest: true,
        include: [{ model: db.Program, required: false, as: "presences" }],
        where: {
            [Op.or]: [{ email: data.login }, { phone: data.login }, { matricule: data.login }],
            [Op.and]: {
                matricule: data.password
            }
        }
    }).then((user) => {
        if (user === null) {
            res.status(401).send({ message: "Unauthorized user null" });
        } else {
            generateToken({ id: user.id }, function(token) {
                res.status(200).send({ token: token, user: user });
            })
        }
    }).catch((err) => {
        res.status(401).json({ error: JSON.stringify(err), message: 'Catch errror' })
    })

})

export default router;