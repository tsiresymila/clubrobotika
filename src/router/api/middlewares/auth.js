import validateToken from '../../../core/validate_token'
import db from '../../../models';
module.exports = (req, res, next) => {
    res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    if (req.path === "/login") {
        next();
    } else {
        validateToken(req, (decoded) => {
            db.Student.findByPk(decoded.id).then((user) => {
                if (user != null) {
                    req.user = user
                    next();
                } else {
                    console.log('Unauthorized user null!')
                    res.status(401).send({
                        message: "Unauthorized user null!"
                    });
                }
            }).catch((err) => {
                res.status(404).send({ message: err });
            })

        }, (error) => {
            console.log("Unauthorized!" + error)
            res.status(401).send({
                message: "Unauthorized!" + error
            });
        })

    }
};