import express from 'express';
import crypto from 'crypto';
import db from '../../models';
import { Op } from "sequelize";
import generateToken from '../../core/generate_token';


let router = express.Router();

router.get('/program', (req, res) => {
    db.Program.findAll({
            where: {
                category: req.user.category
            },
            include: [{
                    model: db.File,
                    required: false,
                    as: "files"
                },
                {
                    model: db.Student,
                    required: false,
                    as: "presents",
                    where: { id: req.user.id },

                },
                { model: db.Exercice, required: false, as: "exercices" }
            ],
            order: [
                ["numero", "ASC"]
            ]
        })
        .then((data) => {
            res.status(200).json({ data: data })
        })
        .catch((err) => {
            res.status(403).send({ message: err })
        })
})

router.post('/program/pourcent', (req, res) => {
    var data = req.body;
    db.PStudent.update(data, { where: { programid: data.programid, studentid: data.studentid } }).then((value) => {
        res.status(200).json({ data: data })
    }).catch(() => {
        res.status(403).send({ message: err })
    })
})
router.post('/add/exercice', (req, res) => {
    var data = {
        programid: req.body.id,
        studentid: req.user.id
    };
    var files = req.files;
    if (files != null) {
        var uploadPath = __dirname + '/../../../public/assets/exerices/' + files.file.name;
        imagefile.file.mv(uploadPath, (err) => {
            if (err) return res.status(500).send(err);
            data['file'] = imagefile.file.name;
            db.SExercice.create(data)
                .then((indice) => {
                    res.status(200).json({ message: "Exercice envoyé", data: data });
                })
                .catch((err) => {
                    res.status(500).json({ message: err })
                })
        });
    } else {
        res.status(500).json({ message: "Fichier obligatoire" })
    }
})

export default router;