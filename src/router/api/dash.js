import express from 'express';
import db from '../../models';
import { Op } from "sequelize";


let router = express.Router();

router.get('/dashboard', (req, res) => {
    db.Notification.findAll({
            where: {
                category: req.user.category
            },
        })
        .then((notification) => {
            db.Program.count({
                    where: {
                        category: req.user.category
                    },
                })
                .then((p) => {
                    db.Student.count({
                            where: {
                                category: req.user.category
                            },
                        })
                        .then((s) => {
                            var data = { notification: notification, p: p, s: s }
                            console.log(data);
                            res.status(200).json(data)
                        })
                        .catch((err) => {
                            res.status(403).send({ message: err })
                        })
                })
                .catch((err) => {
                    res.status(403).send({ message: err })
                })
        })
        .catch((err) => {
            res.status(403).send({ message: err })
        })

})


export default router;