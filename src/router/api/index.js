var express = require('express')
var router = express.Router();
import Auth from './middlewares/auth';
import Login from './login';
import Program from './program';
import Dashboard from './dash';
import Profile from './profile';

//A pi authentification
router.use(Auth);
router.use(Login);
router.use(Dashboard);
router.use(Program);
router.use(Profile);

export default router;