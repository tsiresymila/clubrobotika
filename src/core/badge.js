const PDFDocument = require('pdfkit');
const fs = require('fs');
import path from 'path';
const { uuid } = require('uuidv4');
var badge  = (res, user, qrcode, callback, isStudent = false,expiration="",send=false) => {

    const doc = new PDFDocument({
        layout: 'landscape',
        bufferPages: true,
        size: [192, 360],
        margin : 10

    });
    var filename = user.id + '_' + user.lastname.split(' ').join('_') + '_' + user.name.split(' ').join('-') + uuid()+'.pdf';
    doc.rect(0, 0, doc.page.width, doc.page.height).fillColor('#ffffff', 1).fill("#ffffff");
    //background
    doc.image(
        __dirname + '/../../public/assets/fond.jpg', 10,
        10, {
            cover: [doc.page.width-20, 60],
            align: 'center',
        }
    );
    //logo 
    doc.image(
        __dirname + '/../../public/assets/logo2.png', 285,
        10, {
            fit: [50, 50],
            align: 'center',
        }
    );
    doc.rect(0, 60, doc.page.width, doc.page.height).fillColor('#ffffff', 1).fill();

    // Name
    doc.fontSize(12)
        .fill('#E04343')
        .font(__dirname + '/../../public/fonts/Montserrat-Bold.woff')
        .text(user.name.substring(0,28).toUpperCase(), 110, 75, {
            width: 245,
        });
    // lastname
    doc.fontSize(9)
        .fill('#41403E')
        .text(user.lastname.substring(0,30).replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase()), 110, 90, {
            width: 245,
        });

    // Catgorie
    doc.fontSize(9)
        .fill('#E04343')
    .text("Catégorie  :", 25, 120, {
        width: 60,
         align:'justify'
    })
    doc.fontSize(9)
        .fill('#41403E')
    .text(user.category, 25, 133, {
        width: 40,
        align: "center"
    })

    // type
    doc.fontSize(9)
        .fill('#E04343')
    .text("Matricule :", 25, 150, {
        width: 60,
        align:'justify'
    })
    doc.fontSize(9)
        .fill('#41403E')
    .text(user.matricule, 25, 163, {
        width: 40,
        align: "center"
    })

    // Date d'admission
    doc.fontSize(9)
        .fill('#E04343')
    .text("Occupation : ", 100, 120, {
        width: 60,
         align:'justify'
    })
    doc.fontSize(9)
        .fill('#41403E')
    .text("Membre", 100, 133, {
        width: 60,
        align: "justify"
    })

    // Date d'expiration
    doc.fontSize(9)
        .fill('#E04343')
    .text("Expiration :", 100, 150, {
        width: 120,
        align:'justify'
    })
    doc.fontSize(9)
        .fill('#41403E')
    .text(expiration, 100, 163, {
        width: 60,
        align: "justify"
    })

    doc.image(
        qrcode, 280,
        120, {
                fit: [60, 60],
                align: 'center',
        }
    );

    //photo
    var photo = isStudent ? __dirname + '/../../public/assets/profile/students/' + user.image : __dirname + '/../../public/assets/profile/' + user.image
    if (fs.existsSync(photo)) {
        doc.save();
        doc.rect(25,40,60,60).clip();
        doc.image(
            photo, 25,
            40, {
                cover: [60, 60],
                align: 'center',
                valign: 'center',
            }
        )
    } else {
        doc.image(
            __dirname + '/../../public/assets/robot.jpg', 25,
            40, {
                fit: [60, 60],
                align: 'center',
            }
        );
    }
    // image border
     doc.rect(25, 40, 60, 60).stroke("#E05151");

    if(send){
         res.writeHead(200, {
            'Content-Type': "application/pdf",
            'Content-Disposition': 'attachment;filename=' + filename
        });
        doc.pipe(res);
        doc.end();
    }
    else{
        var pathfile = path.join(__dirname , '/../../public/assets/badges/'+filename);
        doc.pipe(fs.createWriteStream(pathfile));
        doc.end();
        doc.on('finish', function() {
            // what you want to do with the file.
        });
        if(callback){
            callback(pathfile)
        }
    }
};

export default badge;