var path = require('path');
var fs = require('fs');
const { uuid } = require('uuidv4');
// import badgeCreator from '../core/pdf';
import badgeCreator from './badge';
import QRCode from 'qrcode';
var recursive = (res,expiration,stds,indice,data,callback)=>{
    if(indice < stds.length -1){
        var std = stds[indice];
         var badge_name = std.id + '_' + std.lastname.split(' ').join('_') + '_' + std.name.split(' ').join('_');
            badge_name = badge_name.replace('-','');
            var qrfile =  path.join(__dirname ,'/../../public/assets/badges/',badge_name+uuid()+'_qrcode.png')
            QRCode.toFile(qrfile, std.matricule, {
                color: {
                    // dark: "#E04343",
                    light: "#fff"
                },
                version: 5
            }, function(error) {
                if (error){
                    //  res.status(500).send(error);
                    recursive (res,expiration,stds,indice+1,data,callback)
                }
                else {
                    badgeCreator(res, std, qrfile, function(filepath) {
                        data.push({
                            "path" : filepath,name:badge_name+'.pdf'
                        })
                        recursive (res,expiration,stds,indice+1,data,callback)
                    }, true,expiration);
                }
            });

    }
    else{
        callback(data);
    }

}

export default recursive;