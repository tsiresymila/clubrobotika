const ExcelJS = require('exceljs');
const fs = require('fs');
import path from 'path';

function recusiveUser(users, index, worksheet, callback) {
    var user = users[index];
    worksheet.addRow({
        matricule: user.matricule,
        category: user.category,
        name: user.name + " " + user.lastname
    }).commit();
    index = index + 1;
    if (index < users.length - 1) {
        recusiveUser(users, index, worksheet, callback);
    } else {
        if (callback) callback(worksheet);
    }

}
var list_excel = (users, res, callback) => {

    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Liste des etudiants', {
        pageSetup: { paperSize: 9, orientation: 'landscape' }
    });
    worksheet.columns = [{
            key: 'matricule',
            header: 'Matricule',
            width: 40,
        },
        {
            key: 'category',
            header: 'Catégorie',
            width: 40,
        },
        {
            key: 'name',
            header: 'Nom et Prénom',
            align: "left",
            width: 100,
        },
    ];

    // doc.addPage();
    recusiveUser(users, 0, worksheet, function(sheet) {
        // worksheet.commit();
        var filename = "list_etudiant.xlsx";
        res.writeHead(200, {
            'Content-Type': "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            'Content-Disposition': 'filename=' + filename
        });
        workbook.xlsx.write(res)
            .then(function(data) {
                res.end();
            });

    })
};

export default list_excel;