import JWT from 'jsonwebtoken';
module.exports = (req, callback, callbackerror) => {
    let token = req.headers["x-access-token"] || '';
    JWT.verify(token, process.env.TOKEN || 'api_tokey12545kk', (err, decoded) => {
        if (err) callbackerror(err);
        else callback(decoded);
    });
}