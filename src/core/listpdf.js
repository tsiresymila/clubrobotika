const PDFDocument = require('pdfkit');
const PdfTable = require('voilab-pdf-table');
const fs = require('fs');
import path from 'path';

function recusiveUser(users, index, tableBody, callback, isSign) {
    var user = users[index];
    var element = {
        matricule: user.matricule,
        category: user.category,
        name: user.name + " " + user.lastname
    }
    if (isSign) {
        element['sign'] = ""
    }
    tableBody.push(element);
    index = index + 1;
    if (index < users.length - 1) {
        recusiveUser(users, index, tableBody, callback, isSign);
    } else {
        if (callback) callback(tableBody);
    }

}
var ListPdf = (users, res, isSign, callback) => {

    const doc = new PDFDocument({
        bufferPages: true,
        size: "A4",
        margins: {
            top: 30,
            bottom: 10,
            left: 25,
            right: 20
        }
    });
    var table = new PdfTable(doc, {
        bottomMargin: 30
    });

    var colums = [{
            id: 'matricule',
            header: 'Matricule',
            width: 60,
            align: "left",
            padding: [5, 5, 5, 5],
            headerPadding: [5, 5, 5, 5],
        },
        {
            id: 'category',
            header: 'Catégorie',
            width: 70,
            align: "center",
            padding: [5, 5, 5, 5],
            headerPadding: [5, 5, 5, 5],
        },
        {
            id: 'name',
            header: 'Nom et Prénom',
            align: "left",
            padding: [5, 5, 5, 5],
            headerPadding: [5, 5, 5, 5],
        },
    ];
    if (isSign) {
        colums.push({
            id: 'sign',
            header: 'Signature',
            align: "left",
            width: 70,
            padding: [5, 5, 5, 5],
            headerPadding: [5, 5, 5, 5],
        })
    }

    table
        .addPlugin(new(require('voilab-pdf-table/plugins/fitcolumn'))({
            column: 'name',
        }))
        .setColumnsDefaults({
            align: 'center',
            padding: [0, 5, 5, 5],
            headerPadding: [0, 5, 5, 5],
            height: 18,
            cache: false,


        })
        .setColumnsDefaults({
            headerBorder: 'B',
            align: 'right'
        })
        // add table columns
        .addColumns(colums)
        .onPageAdded(function(tb) {
            tb.addHeader();
        });

    // doc.addPage();
    var tableBody = [];
    recusiveUser(users, 0, tableBody, function(t) {
        table.addBody(t);
        var filename = 'list_student.pdf';
        res.writeHead(200, {
            'Content-Type': "application/pdf",
            'Content-Disposition': 'filename=' + filename
        });
        doc.pipe(res);
        doc.end();

    }, isSign)

};

export default ListPdf;