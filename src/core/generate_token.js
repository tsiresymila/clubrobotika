import JWT from 'jsonwebtoken';
require('dotenv').config()

module.exports = (data, callback) => {
    let token = JWT.sign(data, process.env.TOKEN || 'api_tokey12545kk', { expiresIn: 86400 });
    if (callback) callback(token);
}