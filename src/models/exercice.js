'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {

    class Exercice extends Model {
        static init(sequelize) {
            super.init({
                title: DataTypes.STRING,
                description: DataTypes.STRING,
                programid: {
                    type: DataTypes.INTEGER,
                    primaryKey: true
                },
                openDate: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                closeDate: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                file: DataTypes.STRING,
                createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
            }, {
                sequelize,
                modelName: 'Exercice',
            });
        }
        static associate(models) {
            this.belongsTo(models.Student, {
                through: models.SExercice,
                as: 'student',
                foreignKey: 'exerciceid'
            });
        }
    };
    return Exercice;
};