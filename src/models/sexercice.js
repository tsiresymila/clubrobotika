'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {

    class SExercice extends Model {
        static init(sequelize) {
            super.init({
                exerciceid: {
                    type: DataTypes.INTEGER,
                    primaryKey: true
                },
                studentid: {
                    type: DataTypes.INTEGER,
                    primaryKey: true
                },
                file: DataTypes.STRING,
                createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
            }, {
                sequelize,
                modelName: 'SExercice',
            });
        }
        static associate(models) {

        }
    };
    return SExercice;
};