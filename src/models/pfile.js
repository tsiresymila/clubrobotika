'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {

    class PFile extends Model {
        static init(sequelize) {
            super.init({
                type: DataTypes.STRING,
                createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
            }, {
                sequelize,
                modelName: 'PFile',
            });
        }
        static associate(models) {

        }
    };
    return PFile;
};