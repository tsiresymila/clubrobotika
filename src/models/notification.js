'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {

    class Notification extends Model {
        static init(sequelize) {
            super.init({
                title: DataTypes.STRING,
                subtitle: DataTypes.STRING,
                description: DataTypes.STRING,
                category: DataTypes.STRING,
                createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
            }, {
                sequelize,
                modelName: 'Notification',
            });
        }
        static associate(models) {
            // this.belongsTo(models.Program, {
            //     through: models.PExercice,
            //     as: 'program',
            //     foreignKey: 'exerciceid'
            // });
        }
    };
    return Notification;
};