'use strict';
import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {

    class Event extends Model {
        static init(sequelize) {
            super.init({
                title: DataTypes.STRING,
                description: DataTypes.STRING,
                category: DataTypes.STRING,
                date: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                createdAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
                updatedAt: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
            }, {
                sequelize,
                modelName: 'Event',
            });
        }
        static associate(models) {

        }
    };
    return Event;
};