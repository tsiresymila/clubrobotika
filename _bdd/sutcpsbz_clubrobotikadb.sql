-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 16 juil. 2021 à 22:13
-- Version du serveur :  10.2.39-MariaDB
-- Version de PHP : 7.3.28

SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sutcpsbz_clubrobotikadb`
--

-- --------------------------------------------------------

--
-- Structure de la table `Coaches`
--

CREATE TABLE `Coaches` (
  `id` int(11) NOT NULL,
  `userId` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `matricule` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `present` int(11) DEFAULT NULL,
  `missing` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Coaches`
--

INSERT INTO `Coaches` (`id`, `userId`, `username`, `name`, `lastname`, `phone`, `email`, `matricule`, `category`, `image`, `password`, `role`, `present`, `missing`, `createdAt`, `updatedAt`) VALUES
(1, NULL, 'tsiresy', 'RANDRIARIMANANA', 'Tsiresy Milà', '0342083574', 'tsiresymila@gmail.com', 'CO-0005', 'B', '118286803_2392138921095172_3868804958543357555_n.jpg', 'dab90a4d7506f95ef04e10947361b2fc78954d624b471051ba879343b3d909c4', 'superadmin', NULL, NULL, NULL, '2021-03-26 05:52:00'),
(2, NULL, 'lorie', 'RAZANAKOTO ', 'Fenohasontsoa Marilia Lauriane', '0341328722', 'lorierazanakoto35@gmail.com', 'CO-0008', 'A', 'l.jpg', 'ac8a43e203660476b7627030f5c195616fd523f6c61bd6007faad3d2a05219c2', 'superadmin', NULL, NULL, '2021-03-11 10:43:29', '2021-03-22 13:53:25'),
(3, NULL, 'nomentsoa', 'NIAVOARINDRANTO', 'Nomentsoa Zo Lalaina', '', '', 'C0-0014', 'Jeux Vidéo', 'nomena.jpg', 'b818e33538aebfdf33dea11fb029e90506fd5e8398482fe90be25457eecaef7a', 'coach', NULL, NULL, '2021-03-11 10:44:48', '2021-03-12 06:52:18'),
(4, NULL, 'tanjona', 'ANDRIANARISOA', 'Tanjoniaina Miarinaja', '+261340130001', 'tanjona.andrianarisoa@outlook.com', 'C0-0013', 'A', 'tanjona.jpg', 'a116061095aaa0b30b729856b644ae1a2c0e618135314e8e0437688892cc4fe7', 'coach', NULL, NULL, '2021-03-11 10:45:47', '2021-03-12 07:12:26'),
(5, NULL, 'initia', 'RAKOTONDRASOA', 'Heriniaina Stéphanie', '0342750782', 'stephrak2000@gmail.com', 'C0-0015', 'A', 'initia.jpg', '5db1442e6dc0a8bd5d9712fddb0c6b24603fb6d5a5a26be81a7c3567cc254f55', 'coach', NULL, NULL, '2021-03-11 10:46:55', '2021-03-12 07:55:49'),
(6, NULL, 'efraime', 'ANDRIMIJORO', 'Heriniavo Efraime', '', '', 'C0-0001', 'B', 'efraime.jpg', 'a3122453a141ce36aa4c383f4b83b286fa276da17b85512affb3781d7024bca7', 'coach', NULL, NULL, '2021-03-11 10:47:44', '2021-03-11 13:35:20'),
(7, NULL, 'mirana', 'RAMIALISOA', 'Francisca Nandrianina', '', '', 'C0-0004', 'B', 'francisca.jpg', 'd3c47fd367d98f80d3d98ba071ece7bfea0036669ceb725a5575b304af925932', 'coach', NULL, NULL, '2021-03-11 10:48:50', '2021-03-13 11:09:15'),
(8, NULL, 'nyaina', 'MALALATIANA', 'Ny Aina', '', '', 'C0-0002', 'A', 'ny.jpg', 'e81241342a4c550bbcd391a448f1a8b20dc97d303a7e45fff2ca8032a43db1f0', 'coach', NULL, NULL, '2021-03-11 10:49:51', '2021-03-11 13:35:47'),
(9, NULL, 'christian', 'RAZAKAMAHERY', 'Nomena Christian', '+261342906058', 'christiangrzk@gmail.com', 'C0-0007', 'Jeux Vidéo', 'PhotoProfilChristian.jpg', '62bdfec06a495cb4f3d7fd96715c9f01dd49a79952d3bfb169b20d8a503b0567', 'coach', NULL, NULL, '2021-03-11 10:51:07', '2021-03-13 06:39:42'),
(10, NULL, 'maeva', 'RAZAFIMANJATO', 'Maevasoa Irina Gabriela', '', '', 'C0-0018', 'C', 'maeva.jpg', '5234c841d99737c74c1935277a38743829c8104ac8239d6795ac16df91d94ad4', 'coach', NULL, NULL, '2021-03-11 10:51:45', '2021-03-12 16:47:08'),
(11, NULL, 'francilia', 'SOAZARA ', 'Mabelle Francilia', '0341216776', 'mabellesoazara@gmail.com', 'C0-0009', 'B', 'fancilia.jpg', '', 'admin', NULL, NULL, '2021-03-11 10:53:04', '2021-03-20 09:34:01'),
(12, NULL, 'manu', 'RAFELIARIMANANA', 'Sandratriniaina Manuella', '', '', 'C0-0003', 'B', 'manu.jpg', 'dbde6e431edd7f4672f039680c58d4a0b59bff2dacfa25d63a228ba2ce392bd1', 'coach', NULL, NULL, '2021-03-11 10:53:59', '2021-03-11 14:06:41'),
(13, NULL, 'jimrjboys', 'RASOANAIVO', 'Andry Jimmy', '+261 34 72 707 37 ', 'andryjimmyras@gmail.com', 'C0-0017', 'C', 'jim.jpg', '3a3f62a03fac5dc2e2466e43446dd1413de208c47f0c6fa7cedacbfc759ec8c4', 'coach', NULL, NULL, '2021-03-11 10:54:42', '2021-03-26 10:15:00'),
(14, NULL, 'mickaella', 'RASAMIMANANA', 'Fanjatiana Mickaelle', '0349367877', 'Mickaella19.mg@gmail.com', 'C0-0006', 'C', 'mickaela.jpg', '5f3f8c20966c851a88d29d9f80ed22e47d7c2e6f9fa815abd79743bceb2a1419', 'admin', NULL, NULL, '2021-03-11 10:55:24', '2021-04-17 07:34:36'),
(15, NULL, 'Rojo Tsiverisoa', 'RAKOTOVOLOLONA', 'Tsiverisoa Rojotiana', '0327330257 / 0343274480', 'rojotiana.tsiverisoa@gmail.com', 'CO-0019', 'B', 'rojo1.jpg', '3277afdadc8c05e80c6488af62ebb0faedc90f77a2ad8eafc1924b5831ab78ce', 'coach', NULL, NULL, '2021-03-12 08:21:28', '2021-07-11 14:34:20');

-- --------------------------------------------------------

--
-- Structure de la table `Cprograms`
--

CREATE TABLE `Cprograms` (
  `programid` int(11) NOT NULL,
  `coachid` int(11) NOT NULL,
  `finish` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Cprograms`
--

INSERT INTO `Cprograms` (`programid`, `coachid`, `finish`, `createdAt`, `updatedAt`) VALUES
(1, 2, 1, '2021-03-19 10:53:17', '2021-03-19 10:53:17'),
(1, 8, 1, '2021-03-19 18:01:49', '2021-03-19 18:01:49'),
(2, 1, 1, '2021-03-19 20:28:04', '2021-03-19 20:28:04'),
(2, 12, 1, '2021-03-19 20:26:53', '2021-03-19 20:26:53'),
(3, 10, 1, '2021-03-18 14:01:40', '2021-03-18 14:01:40'),
(3, 13, 1, '2021-03-27 08:28:55', '2021-03-27 08:28:55'),
(3, 14, 1, '2021-03-26 10:01:00', '2021-03-26 10:01:00'),
(5, 1, 1, '2021-03-22 13:51:13', '2021-03-22 13:51:13'),
(8, 13, 1, '2021-03-27 08:28:59', '2021-03-27 08:28:59'),
(8, 14, 1, '2021-06-12 06:13:01', '2021-06-12 06:13:01');

-- --------------------------------------------------------

--
-- Structure de la table `Events`
--

CREATE TABLE `Events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Events`
--

INSERT INTO `Events` (`id`, `title`, `description`, `category`, `date`, `createdAt`, `updatedAt`) VALUES
(1, 'Exercice à rendre', '- Exercice', 'A', '2021-05-05 18:28:06', '2021-05-05 18:28:06', '2021-05-05 18:28:06');

-- --------------------------------------------------------

--
-- Structure de la table `Exercices`
--

CREATE TABLE `Exercices` (
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `programid` int(11) NOT NULL,
  `openDate` datetime DEFAULT NULL,
  `closeDate` datetime DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `exerciceid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Exercices`
--

INSERT INTO `Exercices` (`title`, `description`, `programid`, `openDate`, `closeDate`, `file`, `createdAt`, `updatedAt`, `exerciceid`) VALUES
('Exericice 1', '- Exercice', 1, '2021-05-05 18:28:06', '2021-05-05 18:28:06', 'cours.pdf', '2021-05-05 18:28:06', '2021-05-05 18:28:06', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Fees`
--

CREATE TABLE `Fees` (
  `id` int(11) NOT NULL,
  `firstmonth` varchar(255) DEFAULT NULL,
  `secondmonth` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Fees`
--

INSERT INTO `Fees` (`id`, `firstmonth`, `secondmonth`, `createdAt`, `updatedAt`) VALUES
(1, 'Mars', 'Juin', '2021-03-18 13:58:54', '2021-06-19 09:27:39'),
(2, 'Juillet', 'Aout', '2021-07-03 07:55:09', '2021-07-03 07:55:09');

-- --------------------------------------------------------

--
-- Structure de la table `Files`
--

CREATE TABLE `Files` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `securename` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `file` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Files`
--

INSERT INTO `Files` (`id`, `name`, `securename`, `type`, `size`, `createdAt`, `updatedAt`, `file`) VALUES
(7, '39e3cbd9-5e61-4f88-9e5f-60ce792f434f.ebm', 'Video', 'video/webm', '1090514', '2021-03-11 13:41:18', '2021-03-12 06:44:39', NULL),
(8, '082a5e61-f495-4ab9-ac56-5aafedf6247c.mp4', 'Vidéo Algorithme définition\'', 'video/mp4', '11676740', '2021-03-12 06:06:42', '2021-03-12 16:35:04', NULL),
(9, '1303475d-2bdb-4a69-aa5c-f891f1418f6d.mp4', 'Algorithme et programmation', 'video/mp4', '20510953', '2021-03-12 06:17:51', '2021-03-12 16:35:04', NULL),
(10, 'aeed024a-5533-4e4d-9132-4b7003fa80ec.pdf', 'Rappel Algorithme et programmation', 'application/pdf', '313872', '2021-03-12 06:23:02', '2021-03-12 16:35:04', NULL),
(11, '3dc981e6-6855-4f14-927d-5997f2d6d913.pdf', 'electronique', 'application/pdf', '363527', '2021-03-12 16:26:45', '2021-03-12 16:27:27', NULL),
(12, 'aacf3f76-c615-4bb7-b02c-f735fa11e0b9.pdf', 'Introduction ', 'application/pdf', '694075', '0021-03-12 06:45:09', '2021-03-12 16:33:19', NULL),
(13, 'faf951af-9d2c-44ce-96da-848ac742c73f.pdf', 'Robot', 'application/pdf', '325778', '2021-03-12 06:45:42', '2021-03-12 16:32:31', NULL),
(14, 'b4c1e925-fdb9-4a5a-99af-924a4b8a610f.pdf', 'Environnement Android', 'application/pdf', '330702', '2021-03-12 06:28:42', '2021-03-12 06:28:42', NULL),
(15, '627743a1-702e-468e-a14a-8a867b3246a0.pdf', 'exerciceresistance ', 'application/pdf', '199671', '2021-03-12 16:44:12', '2021-03-12 16:44:31', NULL),
(17, '83ac4fbc-85bd-4e02-95de-9da7f8e2655c.pdf', 'exercicenumérique', 'application/pdf', '20149', '2021-03-19 10:49:34', '2021-03-19 10:54:09', NULL),
(18, '56169628-193a-42e4-a253-47c015847791.pdf', 'electroniquesuite', 'application/pdf', '494581', '2021-03-19 10:50:22', '2021-03-19 10:54:09', NULL),
(19, 'a237cea6-119b-47f2-b91c-47081a49bcbd.pdf', 'Suite cours algorithme', 'application/pdf', '760259', '2021-03-19 19:33:44', '2021-03-19 19:34:50', NULL),
(20, 'ee71df3b-3745-499c-b06e-5e7c34a0cfe3.pdf', 'Correction exercice et nouveau exercice', 'application/pdf', '208311', '2021-03-19 19:34:17', '2021-03-19 19:35:06', NULL),
(22, '4a77efc0-160e-4a9f-b218-493863f40e4b.pdf', 'Rappel Arduino ', 'application/pdf', '382099', '2021-03-26 05:52:54', '2021-03-26 06:04:14', NULL),
(23, '21c742f3-a96c-4223-acef-788c25037b63.pdf', 'Porte Logique', 'application/pdf', '184585', '2021-03-26 06:42:34', '2021-03-26 06:42:48', NULL),
(27, '2757f34b-fc4a-46f0-b61e-75b57b3d9443.pdf', 'MIT AI2 COURS 1 ', 'application/pdf', '1007378', '2021-03-26 09:38:55', '2021-03-26 09:39:13', NULL),
(28, 'ea66793b-a12f-4027-8460-a95abb8e26f9.pdf', 'Boucle, Tableau , Fonction', 'application/pdf', '2069442', '2021-06-18 18:52:54', '2021-06-18 18:52:54', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `FStudents`
--

CREATE TABLE `FStudents` (
  `ispay` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `FeeId` int(11) NOT NULL,
  `StudentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `FStudents`
--

INSERT INTO `FStudents` (`ispay`, `createdAt`, `updatedAt`, `FeeId`, `StudentId`) VALUES
(1, '2021-06-19 09:06:44', '2021-06-19 09:06:44', 1, 51),
(1, '2021-07-03 09:10:48', '2021-07-03 09:10:48', 1, 65),
(NULL, '2021-03-22 13:50:11', '2021-03-22 13:50:28', 1, 134),
(1, '2021-07-03 09:14:13', '2021-07-03 09:14:13', 1, 137),
(1, '2021-06-19 09:16:11', '2021-06-19 09:16:11', 1, 146),
(1, '2021-07-03 07:55:25', '2021-07-03 07:55:25', 2, 156);

-- --------------------------------------------------------

--
-- Structure de la table `Notifications`
--

CREATE TABLE `Notifications` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Notifications`
--

INSERT INTO `Notifications` (`id`, `title`, `subtitle`, `description`, `createdAt`, `updatedAt`, `category`) VALUES
(1, 'Nouveau exercice ajouté', 'Exericice 1', '- Exercice', '2021-05-05 18:28:06', '2021-05-05 18:28:06', 'A'),
(2, 'Nouveau programme ajouté', 'APPLICATION D\'UTILISATION DE BOUCLE , TABLEAU ET FUNCTION', '', '2021-06-18 18:22:49', '2021-06-18 18:22:49', 'B'),
(3, 'Nouveau programme ajouté', 'SUITE FONCTION TABLEAU ', '', '2021-07-02 14:03:50', '2021-07-02 14:03:50', 'B');

-- --------------------------------------------------------

--
-- Structure de la table `PFiles`
--

CREATE TABLE `PFiles` (
  `type` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `programid` int(11) NOT NULL,
  `FileId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `PFiles`
--

INSERT INTO `PFiles` (`type`, `createdAt`, `updatedAt`, `programid`, `FileId`) VALUES
('lesson', '2021-05-05 17:19:47', '2021-05-05 17:19:47', 1, 8),
('lesson', '2021-05-05 17:20:05', '2021-05-05 17:20:05', 1, 12),
('lesson', '2021-07-10 07:45:47', '2021-07-10 07:45:47', 2, 14),
('lesson', '2021-06-12 11:03:30', '2021-06-12 11:03:30', 6, 22),
('lesson', '2021-07-02 14:04:52', '2021-07-02 14:04:52', 10, 28);

-- --------------------------------------------------------

--
-- Structure de la table `Programs`
--

CREATE TABLE `Programs` (
  `id` int(11) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `finish` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Programs`
--

INSERT INTO `Programs` (`id`, `category`, `title`, `description`, `numero`, `date`, `finish`, `createdAt`, `updatedAt`, `image`) VALUES
(1, 'A', 'INTRODUCTION A LA ROBOTIQUE ET ELECTRONIQUE DE BASE', '-Seul le fichier \"élèctronique \" que l\'on peut projeter aux étudiants<br>\r\n- Effectue l\'introduction à la robotique et domotique<br>\r\n- Citez des différents application de la robotique<br>\r\n- Expliquer pourquoi nous offrons la formation pour la robotique ', 1, '2021-03-13 00:00:00', NULL, '2021-03-12 16:25:24', '2021-05-05 18:29:38', 'avisdembauche.jpg'),
(2, 'B', 'INTRODUCTION + RAPPEL ALGORITHMIQUE', '- Introduction générale <br>\r\n- Introduction et illustration d\'application d\'algorithme dans le domaine robotique <br>\r\n- Rappel sur l\'algorithmique', 1, '2021-03-13 00:00:00', NULL, '2021-03-11 11:15:45', '2021-03-12 06:51:47', NULL),
(3, 'C', 'INTRODUCTION À L\'ENVIRONNEMENT ANDROID', '- Introduction général du cours <br>\r\n- Explication des objectifs <br>\r\n- Pourquoi devrait utiliser Android<br>\r\n- Introduction à l\'univers Android (cours pdf)', 1, '2021-03-20 00:00:00', NULL, '2021-03-11 18:16:07', '2021-03-19 19:35:29', NULL),
(4, 'A', 'ELECTRONIQUE SUITE ', 'Suite des leçons\r\nexercice sur la base de système numérique ', 2, '2021-03-20 00:00:00', NULL, '2021-03-19 10:53:06', '2021-03-19 10:53:06', NULL),
(5, 'B', 'RAPPEL ALGORITHMIQUE SUITE', '- Correction des exercices hexadécimale en décimale et décimale en binaire<br>\r\n-La structure conditionnelle et itérative<br>\r\n- Utilisation de fonction et procédure<br>\r\n- Exercice cryptage Vigenère', 2, '2021-03-20 00:00:00', NULL, '2021-03-19 19:32:48', '2021-03-19 19:32:48', NULL),
(6, 'B', 'RAPPEL SUR ARDUINO', '- Rappel sur la tension / intensité <br>\r\n- Présentation microcontrôleur et microprocesseur <br>\r\n- Présentation de logiciel Arduino<br>\r\n- Présentation de matériel Arduino <br>\r\n- Rappel sur la programmation ave Arduino<br>', 3, '2021-03-27 00:00:00', NULL, '2021-03-26 05:50:01', '2021-03-26 05:54:15', NULL),
(7, 'A', 'ALGEBRE DE BOOLE ET LES PORTES LOGIQUES', '- Algèbre de Boole <br>\r\n- Opérateur Logiques <br>\r\n- Identité remarquables <br>\r\n- Théorème de Morgan <br>', 3, '2021-03-27 00:00:00', NULL, '2021-03-26 06:42:15', '2021-03-26 06:47:08', NULL),
(8, 'C', 'DEVELOPPEMENT D\'APPLICATION ANDROID AVEC MIT APP INVENTOR 2', '- Introduction de MIT APP INVENTOR<br>\r\n- Présentation de l\'outils APP INVENTOR <br>\r\n- Présentation des widgets <br>\r\n- Interaction des widgets avec le programme bloc <br>\r\n- https://drive.google.com/drive/u/1/folders/19O7vjNwBRiGTsuVLE6nB0w5BGTwjsPX0', 2, '2021-03-27 00:00:00', NULL, '2021-03-26 08:30:14', '2021-03-26 09:53:53', NULL),
(9, 'B', 'APPLICATION D\'UTILISATION DE BOUCLE , TABLEAU ET FUNCTION', '- Exemple et Exercice d\'application de boucle <br>\r\n- Type de donnée tableau <br>\r\n- Utilisation de fonction avec Arduino  <br>', 4, '2021-06-19 00:00:00', NULL, '2021-06-18 18:22:49', '2021-06-18 18:23:15', NULL),
(10, 'B', 'SUITE FONCTION TABLEAU ', '- Introduction sur les fonctions <br>\r\n- Utilisation de tableau <br>\r\n- application <br>', 5, '2021-07-03 00:00:00', NULL, '2021-07-02 14:03:50', '2021-07-02 14:03:50', 'tlchargementj.fif');

-- --------------------------------------------------------

--
-- Structure de la table `PStudents`
--

CREATE TABLE `PStudents` (
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `programid` int(11) NOT NULL,
  `studentid` int(11) NOT NULL,
  `pourcent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `PStudents`
--

INSERT INTO `PStudents` (`createdAt`, `updatedAt`, `programid`, `studentid`, `pourcent`) VALUES
('2021-03-18 14:01:16', '2021-03-18 14:01:16', 3, 129, NULL),
('2021-03-20 13:31:07', '2021-03-20 13:31:07', 5, 9, NULL),
('2021-03-20 09:02:14', '2021-03-20 09:02:14', 5, 11, NULL),
('2021-03-20 13:31:42', '2021-03-20 13:31:42', 5, 25, NULL),
('2021-03-20 13:26:53', '2021-03-20 13:26:53', 5, 28, NULL),
('2021-03-20 13:32:02', '2021-03-20 13:32:02', 5, 30, NULL),
('2021-03-20 13:32:15', '2021-03-20 13:32:15', 5, 34, NULL),
('2021-03-20 13:32:28', '2021-03-20 13:32:28', 5, 36, NULL),
('2021-03-20 13:32:45', '2021-03-20 13:32:45', 5, 40, NULL),
('2021-03-20 13:33:11', '2021-03-20 13:33:11', 5, 45, NULL),
('2021-03-20 13:30:43', '2021-03-20 13:30:43', 5, 49, NULL),
('2021-03-20 13:34:13', '2021-03-20 13:34:13', 5, 51, NULL),
('2021-03-20 13:34:29', '2021-03-20 13:34:29', 5, 68, NULL),
('2021-03-20 13:26:32', '2021-03-20 13:26:32', 5, 70, NULL),
('2021-03-20 13:34:44', '2021-03-20 13:34:44', 5, 73, NULL),
('2021-03-20 13:34:57', '2021-03-20 13:34:57', 5, 81, NULL),
('2021-03-20 13:35:39', '2021-03-20 13:35:39', 5, 97, NULL),
('2021-03-20 13:35:53', '2021-03-20 13:35:53', 5, 98, NULL),
('2021-03-20 13:36:06', '2021-03-20 13:36:06', 5, 99, NULL),
('2021-03-20 13:36:25', '2021-03-20 13:36:25', 5, 103, NULL),
('2021-03-20 13:36:34', '2021-03-20 13:36:34', 5, 111, NULL),
('2021-03-20 13:37:23', '2021-03-20 13:37:23', 5, 120, NULL),
('2021-03-27 08:06:46', '2021-03-27 08:06:46', 7, 5, NULL),
('2021-03-27 08:29:10', '2021-03-27 08:29:10', 8, 141, NULL),
('2021-03-27 08:29:42', '2021-03-27 08:29:42', 8, 142, NULL),
('2021-06-19 09:22:40', '2021-06-19 09:22:40', 9, 111, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SExercices`
--

CREATE TABLE `SExercices` (
  `exerciceid` int(11) NOT NULL,
  `studentid` int(11) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `ExerciceProgramid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Students`
--

CREATE TABLE `Students` (
  `id` int(11) NOT NULL,
  `userId` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `birthhday` varchar(255) DEFAULT NULL,
  `birthplace` varchar(255) DEFAULT NULL,
  `isUniv` tinyint(1) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `field` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `matricule` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `motivation` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `present` int(11) DEFAULT NULL,
  `missing` int(11) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Students`
--

INSERT INTO `Students` (`id`, `userId`, `name`, `lastname`, `phone`, `email`, `birthhday`, `birthplace`, `isUniv`, `grade`, `field`, `school`, `matricule`, `category`, `motivation`, `image`, `password`, `present`, `missing`, `time`, `createdAt`, `updatedAt`) VALUES
(1, '', 'ANDRIAMAHALEO', 'Nomena Fandresena', '', '', NULL, '', 0, '', '', '', 'S2-0001', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-04-20 09:02:44'),
(2, '', 'ANDRIAMAHAZO', 'Robinson Pascal ', '', '', NULL, '', 0, '', '', '', 'S2-0002', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:40:16'),
(3, '', 'ANDRIAMAHEFA ', 'Fitia Faniry ', '', '', NULL, '', 0, '', '', '', 'S2-0003', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-03 09:09:45'),
(4, '', 'ANDRIAMAHERILALA', 'Josia', '0348233625', 'andriamaherilalajosia@gmailcom', NULL, 'Soavinandriana', 0, '1 ère année', 'Génie des sciences et technologies industriels', 'ESPA Vontovorona', 'S2-0004', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-20 13:42:29'),
(5, '', 'ANDRIAMALALA', 'Andry Midera', '0341888578', '', NULL, '', 1, '1ere année', 'Informartique', 'ITU', 'S2-0005', 'A', '', 'Midera.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 10:26:44'),
(6, '', 'ANDRIAMAMPIONONA ', 'Mbolatiana Henintsoa', '0347917696', 'andriamampiononaMbolatiana62@gmail', NULL, 'Ambositra', 1, '3e année', '', 'ESMIA', 'S2-0006', 'A', '', 'Mbolatiana.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 10:36:44'),
(7, '', 'ANDRIAMANALINA', 'RINAH Tatamo', '', '', NULL, '', 0, '', '', '', 'S2-0007', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:41:14'),
(8, '', 'ANDRIAMANARINTSOA', 'Fenohasina Ziana', '', '', NULL, '', 0, '', '', '', 'S2-0008', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:41:26'),
(9, '', 'ANDRIANARIMANANA', 'Arsène Jerry', '0345343018', 'ajerryarsena@gmail.com', NULL, 'ihosy', 0, '', 'informatique generale', '', 'S2-0009', 'B', '', 'jerry.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:51:38'),
(10, '', 'ANDRIANARINAVALONA', 'Thalia Andréa', '', '', NULL, '', 0, '', '', '', 'S2-0010', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 14:18:49'),
(11, '', 'ANDRIANAY ', 'Miarintsoa Orlando ', '', '', NULL, '', 0, '', '', '', 'S2-0011', 'B', '', 'Orlando.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:57:09'),
(12, '', 'ANDRIANIAINA ', 'Ranja Tsilavina ', '', '', NULL, '', 0, '', '', '', 'S2-0012', 'B', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:42:18'),
(13, '', 'ANDRIANIFAHANANA', 'Tsanta Nantenaina ', '', '', NULL, '', 0, '', '', '', 'S2-0013', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-06-19 09:21:43'),
(14, '', 'ANDRIANIMANANA', 'Irina Hendry', '', '', NULL, '', 0, '', '', '', 'S2-0014', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:42:43'),
(15, '', 'ANDRIANOVONA', ' Tianaharivola Germaine', '0346927795', 'andrianovonotiana@gmail.com', NULL, 'Maevatanana', 1, '3e année', '', 'IST Ampasapito', 'S2-0015', 'B', '', 'Tiana.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-20 10:25:52'),
(16, '', 'ANDRIANTSEHENO', 'Mialisoa', '', '', NULL, '', 0, '', '', '', 'S2-0016', 'A', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:43:06'),
(17, '', 'ANJARASOA ', 'Safidy ', '', '', NULL, '', 0, '', '', '', 'S2-0017', 'A', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:43:52'),
(18, '', 'ANSOIREDINE ', 'Ali Soilihi', '', '', NULL, '', 0, '', '', '', 'S2-0018', 'A', '', 'e1.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:12:51'),
(19, '', 'ANTSO HARIMBOLA', 'Johnson', '', '', NULL, '', 0, '', '', '', 'S2-0019', 'A', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:44:04'),
(20, '', 'ARIVONY', 'Ny Antsa Fandresena,', '', '', NULL, '', 0, '', '', '', 'S2-0020', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-01 14:17:20'),
(21, '', 'Arson', 'Felix', '', '', NULL, '', 0, '', '', '', 'S2-0021', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:44:28'),
(22, '', 'BETOKOTANY ', 'Vololoniaina Mialisoa ', '', '', NULL, '', 0, '', '', '', 'S2-0022', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-01 13:57:39'),
(23, '', 'DAMY', 'Joyot Mamet Clotaire', '', '', NULL, '', 0, '', '', '', 'S2-0023', 'B', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:44:52'),
(24, '', 'DIARINIAINA HARIVONY ', 'Fitiavana Zo Sarobidy ', '', '', NULL, '', 0, '', '', '', 'S2-0024', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:45:04'),
(25, '', 'faly nomenjanahary', 'alain zidane', '', '', NULL, '', 0, '', '', '', 'S2-0025', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 14:21:52'),
(26, '', 'FIDIMAMISOA ', 'Tsiferaniaina ', '', '', NULL, '', 0, '', '', '', 'S2-0026', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 14:21:33'),
(27, '', 'FIDIMANANA', 'Zo Lalaina Orlando ', '', '', NULL, '', 0, '', '', '', 'S2-0027', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-01 14:17:55'),
(28, '', 'FISAORANDRAY ', 'Zo Tahina ', '+261349558783', 'fisaorandrayzotahina@gmail.com', NULL, 'Tana', 1, '4e annee', 'Informartique', 'E-Media', 'S2-0028', 'B', '', 'e2.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-20 10:09:58'),
(29, '', 'HARIMALALA', 'santatriniavo salohy Marie Jessica', '', '', NULL, '', 0, '', '', '', 'S2-0029', 'A', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:48:49'),
(30, '', 'MAHASETRA', 'Harivony Nick Christian', '', '', NULL, '', 0, '', '', '', 'S2-0030', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:03:44'),
(31, '', 'MALALANIRINA', 'Santatra Liantsoa', '', '', NULL, '', 0, '', '', '', 'S2-0031', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:49:16'),
(32, '', 'MAMISOANIAINA ', 'Haritiana Sarah ', '', '', NULL, '', 0, '', '', '', 'S2-0032', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-06-19 09:15:41'),
(33, '', 'MANANTSOA', 'Ernest Justin ', '', '', NULL, '', 0, '', '', '', 'S2-0033', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:50:17'),
(34, '', 'MANDRESY', 'Koloina Paulà', '', '', NULL, '', 0, '', '', '', 'S2-0034', 'B', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:51:29'),
(35, 'Mihaja Ezra', 'MIHAJAMANANJARA', 'Ezra Ny Aina ', '0346290972', 'emihaja@gmail.com', NULL, 'Anjanahary ', 1, '2 e annee', 'genie industrielle', 'CNTEMAD', 'S2-0035', 'A', '', 'Ezra.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-14 06:40:58'),
(36, '', 'NIVOSON', ' Fréderic Tahinasoa', '0345953048', 'fredericnivoson@gmail.com', NULL, 'Andrefan`Manjara', 1, '', 'informatique', '', 'S2-0036', 'B', '', 'Frdric.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:46:52'),
(37, '', 'NJARA', 'Fitiavana', '', '', NULL, '', 0, '', '', '', 'S2-0037', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-01 14:21:03'),
(38, '', 'Pelimanana', 'Andriamanga Gael Kenny Ludovic ', '', '', NULL, '', 0, '', '', '', 'S2-0038', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:45:21'),
(40, 'Johary rbm', 'Rabetsimialona', 'Johary Lalaina', '0324555834', 'jrabetsimiavona@gmail.com', NULL, 'ambohimiadana avaratra', 1, '2 e annee', 'Electro-mecanique', 'St Michel', 'S2-0040', 'B', '', 'JoharyLalaina.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 11:26:13'),
(41, 'Syvio Kevin Radafy', 'Radafy', 'Syvio Kevin', '0344379205', 'syviokevinrdf@gmail.com', NULL, 'Antsirabe', 1, '1 ere annee', 'informatique', 'ESTI', 'S2-0041', 'A', '', 'Syvio.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 11:28:36'),
(42, '', 'RAFANOMEZANTSOA', 'Arsene', '', '', NULL, '', 0, '', '', '', 'S2-0042', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-03 09:08:51'),
(43, 'Rafanomezantsoa Pierrot', 'RAFANOMEZANTSOA', 'Pierrot', '0349193169', 'pierrotp19.aps1b@gmail.com', NULL, 'Manakara', 1, '1 ere annee', 'Telecommumications', 'ESPA', 'S2-0043', 'A', 'cotisation  2 mois', 'pierrot.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 09:58:14'),
(44, '', 'RAFANOMEZANTSOA', 'Sitraky Ny Avo,', '', '', NULL, '', 0, '', '', '', 'S2-0044', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 13:58:39'),
(45, '', 'RAFIDIARISON ', 'Safidy', '', '', NULL, '', 0, '', '', '', 'S2-0045', 'B', '', 'Safidy.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 10:06:40'),
(46, '', 'RAFINDRAJERY RASEHENOLAHY ', 'John Oel Mario ', '', '', NULL, '', 0, '', '', '', 'S2-0046', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:59:41'),
(47, '', 'RAHARIMANANA', 'Nirina Georges Mickaël', '', '', NULL, '', 0, '', '', '', 'S2-0047', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:59:47'),
(48, '', 'RAHARINIRINA ', 'Joseba Moria ', '', '', NULL, '', 0, '', '', '', 'S2-0048', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 13:52:47'),
(49, '', 'RAHERINDRAINY ', 'Tohavina Mickael', '', '', NULL, '', 0, '', '', '', 'S2-0049', 'B', '', 'Tohavina.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 10:09:08'),
(50, '', 'Raholiharimalala', 'marie Véronique', '', '', NULL, '', 0, '', '', '', 'S2-0050', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 07:00:05'),
(51, '', 'RAJAOFETRA ', 'Marc Loïc ', '', '', NULL, '', 0, '', '', '', 'S2-0051', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-06-19 09:05:05'),
(52, '', 'Rajaonarivelo', 'Notahina', '', '', NULL, '', 0, '', '', '', 'S2-0052', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:58:46'),
(53, '', 'RAKOTOARINAIVO', 'Diamondra Fandresena Fanomezantsoa.', '', '', NULL, '', 0, '', '', '', 'S2-0053', 'A', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:58:41'),
(54, '', 'RAKOTOARINORO', 'Toavina Kevin', '0340667748', 'rakotoarinorotoavinakevin@yahoo.fr', NULL, 'Soavinandriana', 0, '', '', '', 'S2-0054', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 10:44:30'),
(55, '', 'RAKOTOARISOA', 'Louis Sergio', '', '', NULL, '', 0, '', '', '', 'S2-0055', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:58:25'),
(56, '', 'Rakotoarison', 'Fabien', '', '', NULL, '', 0, '', '', '', 'S2-0056', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:58:21'),
(57, '', 'RAKOTOBE ', 'Mamimpitia Mickaël ', '', '', NULL, '', 0, '', '', '', 'S2-0057', 'B', '', 'e3.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:58:16'),
(58, '', 'Rakotonanahary', 'Mialinirina', '', '', NULL, '', 0, '', '', '', 'S2-0058', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:08:57'),
(59, '', 'RAKOTONANAHARY ', 'Jean Yves ', '', '', NULL, '', 0, '', '', '', 'S2-0059', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:58:11'),
(60, '', 'RAKOTONDRAINIBE', 'Miharivola', '', '', NULL, '', 0, '', '', '', 'S2-0060', 'B', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:56:58'),
(61, '', 'RAKOTONIAINA', 'Ny Lova Nomenjanahary', '', '', NULL, '', 0, '', '', '', 'S2-0061', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:56:50'),
(62, '', 'RAKOTONIAINA ', 'Ny Lova Nomenjanahary', '', '', NULL, '', 0, '', '', '', 'S2-0062', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:56:43'),
(63, '', 'RAKOTONIRINA', 'Fetraharinala Antonio', '', '', NULL, '', 0, '', '', '', 'S2-0063', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:56:38'),
(64, '', 'RAKOTONOMENJANAHARY', 'Lahatry Ny Avo Sergina', '', '', NULL, '', 0, '', '', '', 'S2-0064', 'B', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:56:32'),
(65, '', 'RALAIARINONY ', 'Mirindra Nomena Johan ', '', '', NULL, '', 0, '', '', '', 'S2-0065', 'A', '', 'Johan.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:53:14'),
(67, '', 'Ramamonjisoa', 'Heriniaina Fanomezantsoa', '', '', NULL, '', 0, '', '', '', 'S2-0067', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:56:09'),
(68, '', 'RAMANAMISATA', 'Laurène ', '', '', NULL, '', 0, '', '', '', 'S2-0068', 'B', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:56:04'),
(69, '', 'RAMANANDRAIBE', 'Meja Tojo Kantoniaina', '', '', NULL, '', 0, '', '', '', 'S2-0069', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:55:55'),
(70, 'RAMANANJATO Gianie', 'RAMANANJATO', 'Gianie', '0349794954', 'gianierama@gmail.com', NULL, 'Soavinandriana', 1, '2 e annee', 'informatique', 'CNTEMAD', 'S2-0070', 'B', '', 'gianie.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 11:22:46'),
(71, '', 'RAMANANTSIHOARANA', 'Safidy', '', '', NULL, '', 0, '', '', '', 'S2-0071', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:55:42'),
(72, '', 'RAMANJATOSOA ', 'Rova Haridina ', '0344864744', '', NULL, '67 Ha', 0, 'Master 2', 'Sciences cognitives', 'ESPA Vontovorona', 'S2-0072', 'B', '', 'IMG_20210320_165518.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-21 10:05:26'),
(73, '', 'RAMARA', 'Dimby Jean Esperant ', '', '', NULL, '', 0, '', '', '', 'S2-0073', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 14:19:28'),
(74, '', 'RAMAROSON', 'Mandaniarivo Nasolo,', '', '', NULL, '', 0, '', '', '', 'S2-0074', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:54:40'),
(75, '', 'RAMAROSON', 'Rabevoninahitriarivomanana Josoa', '0346927795', 'andriavonatiana@gmail.com', NULL, 'Maevatanana', 1, '1ere année', '', '', 'S2-0075', 'A', '', 'Josoa.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 10:18:49'),
(76, '', 'RAMBELOARISON', ' Kanto Manantsoa ', '', '', NULL, '', 0, '', '', '', 'S2-0076', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 13:57:23'),
(77, '', 'RAMBELOSON ', 'Tiavina ', '', '', NULL, '', 0, '', '', '', 'S2-0077', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-01 14:17:40'),
(78, '', 'RAMIADAMITOVOMANANA', 'Onja Fiderana', '', '', NULL, '', 0, '', '', '', 'S2-0078', 'A', '', '216358741_404495394307523_5013902929000465700_n.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-16 18:50:33'),
(79, '', 'RAMIANDRISOA', 'Harilalaina Stephan', '0347917696', 'andriamampiononambolatiana62@gmail.com', NULL, 'Ambositra', 0, '', '', '', 'S2-0079', 'A', '', 'Stephan.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-20 10:40:15'),
(80, '', 'RAMIANDRISOA', 'Marie Anita', '', '', NULL, '', 0, '', '', '', 'S2-0080', 'B', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:54:07'),
(81, '', 'RAMIHAJARIMANANA', 'Herijaona Mampionona,', '0349809223', '', NULL, 'Manjakandriana', 1, '3e annee', 'informatique', 'INATA', 'S2-0081', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 10:02:37'),
(82, '', 'RAMILIARISON', ' Fanomezantsoa François ', '0349067754', 'RamiliarisonFano21@gmail.com', NULL, '', 0, '3e année', 'BTP', 'U-magis', 'S2-0082', 'A', '', 'Fanomezantsoa.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 10:20:20'),
(83, '', 'Ramisarivo ', 'Tiavina', '', '', NULL, '', 0, '', '', '', 'S2-0083', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:53:54'),
(84, '', 'ranaivojaonajudicael', '', '', '', NULL, '', 0, '', '', '', 'S2-0084', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:53:38'),
(85, '', 'RANDRIAHARIMALALA', 'Elio', '', '', NULL, '', 0, '', '', '', 'S2-0085', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:53:32'),
(86, 'Gilberto Randriamamory', 'RANDRIAMAMORY ', 'Mampihentse Gilberto ', '0325040579', 'Gilbertorandriamamory@gmail.com', NULL, 'morondava', 1, '1 ere annee', 'GPCI', 'ESPA', 'S2-0086', 'A', '', 'gilberto.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 11:24:55'),
(87, '', 'Randriamiandrisoa', 'Tsiory Stephanie', '', '', NULL, '', 0, '', '', '', 'S2-0087', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-07-01 14:20:43'),
(89, '', 'Randrianalisedra', 'Oniaina Shamia ', '', '', NULL, '', 0, '', '', '', 'S2-0089', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:53:02'),
(90, '', 'RANDRIANALITERA', 'Tsiky Haritiana Fanantenana ', '', '', NULL, '', 0, '', '', '', 'S2-0090', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:52:51'),
(91, '', 'RANDRIANANTENAINA', 'Emmanuel Éric', '', '', NULL, '', 0, '', '', '', 'S2-0091', 'B', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:52:40'),
(92, '', 'RANDRIANARIJAONA', 'Santatriniaina Princia ', '', '', NULL, '', 0, '', '', '', 'S2-0092', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 13:58:55'),
(93, '', 'RANDRIANARISOA', 'Liantsoa', '', '', NULL, '', 0, '', '', '', 'S2-0093', 'B', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 09:30:30'),
(94, '', 'RANDRIANARISON', 'Lovatiana Andrianina Nancy', '', '', NULL, '', 0, '', '', '', 'S2-0094', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:47:43'),
(95, '', 'RANDRIANARIVO', 'Tianariliva Tendry', '', '', NULL, '', 0, '', '', '', 'S2-0095', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 14:18:28'),
(96, '', 'RANDRIANASOLO ', 'Njaka Nirina Tommy', '', '', NULL, '', 0, '', '', '', 'S2-0096', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:49:15'),
(97, '', 'RASAMBOMANANA', 'Henintsoa Maillard Modeste', '', '', NULL, '', 0, '', '', '', 'S2-0097', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 14:21:21'),
(98, '', 'RASAMOELY ', 'Kiady Andriampianinana', '', '', NULL, '', 0, '', '', '', 'S2-0098', 'B', '', 'kiady.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:53:44'),
(99, '', 'RASETRISOLONDRANAIVO', 'Rija', '', '', NULL, '', 0, '', '', '', 'S2-0099', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 14:19:11'),
(100, '', 'RASOLOMANANJATOVO', 'Fanantenana Michaël', '', '', NULL, '', 0, '', '', '', 'S2-0100', 'B', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:48:51'),
(101, '', 'RATOVOARIMANANA', 'Fetrasoa', '', '', NULL, '', 0, '', '', '', 'S2-0101', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:48:46'),
(102, '', 'RAVELOMANANTSOA ', 'Raina Nomena Erico ', '', '', NULL, '', 0, '', '', '', 'S2-0102', 'A', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:48:41'),
(103, '', 'RAVELONJOHANISON  RAZANADRABE', 'Angélica Hajaina', '0327043261', 'angelicaravelojohanison@gmail.com', NULL, 'sonierana', 1, '', 'informatique', '', 'S2-0103', 'B', '', 'Angelica.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 09:45:04'),
(104, '', 'RAZAFIARISOA', 'Vonihasina', '', '', NULL, '', 0, '', '', '', 'S2-0104', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-15 12:16:37'),
(105, '', 'Razafimanantsoa', 'Voahary  Fitahiana', '', 'Voaryrazafy@gmail.com', NULL, 'Anjanahary', 0, '1 ère année', 'Génie des procédés industriels', 'ESPA Vontovorona', 'S2-0105', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-20 13:39:02'),
(106, '', 'RAZAFIMANANTSOA ', 'Koloina ', '', 'mi-koloina-9@gmail.com', NULL, 'Andravoahangy', 1, '1 ere annee', 'Mathematiques', 'ITU', 'S2-0106', 'A', '', 'Koloina.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 10:24:33'),
(107, '', 'RAZAFINANTOANINA', 'Miharimanjato', '0343194036', 'razafinantoaninamiharimanjato@gmail.com', NULL, 'Antananarivo', 0, '1 ère année', 'Electronique et système informatique', 'ISPM', 'S2-0107', 'B', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-20 13:45:14'),
(108, '', 'RAZAFINDRABE', 'Solonirina Nysa Fanomezana,', '', '', NULL, '', 0, '', '', '', 'S2-0108', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 13:54:24'),
(109, '', 'RAZAFINDRABE ', 'Solonirina Sandratriniavo ', '', '', NULL, '', 0, '', '', '', 'S2-0109', 'A', '', 'IMG_20210320_162344.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-21 10:00:45'),
(110, '', 'RAZAFINDRAJERY', 'Rasehenolahy John Oel Mario', '0325754027', '', NULL, 'Itaosy', 1, '', 'Telecommumications', 'CNTEMAD', 'S2-0110', 'B', '', 'Mario.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 09:54:21'),
(111, '', 'RAZAFINDRAKOTO', 'Fenohery Mickael', '0340936251', '', NULL, '', 0, '', '', '', 'S2-0112', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 08:58:20'),
(112, 'Hilariot', 'RAZAFINDRAMANANA ', 'Nasolo Hilariot Jenny ', '0345511927', '', NULL, 'Ankadifotsy', 0, 'bacc', '', '', 'S2-0113', 'A', '', 'nasolo.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 11:26:57'),
(113, '', 'Razakandriarimanana', 'Romeo Erika ', '', '', NULL, '', 0, '', '', '', 'S2-0114', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 13:52:25'),
(114, '', 'RAZAMAJATOVO ', 'Ornella Princy ', '', '', NULL, '', 0, '', '', '', 'S2-0115', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 13:56:11'),
(115, '', 'Razanamparany', 'Anjara Tsilavina', '', '', NULL, '', 0, '', '', '', 'S2-0116', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:46:34'),
(116, '', 'Razanaparany', 'Miora Joëlle Hariliva ', '', '', NULL, '', 0, '', '', '', 'S2-0117', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-07-01 13:56:27'),
(117, 'tsiory robson', 'Robsomanitrandrasana', 'Tsiorisoa Gaelle', '0342678575', 'tsioryrobson@gmail.com', NULL, 'Antsirabe', 1, '1 ere annee', 'informatique', 'ESUM', 'S2-0118', 'A', '', 'Tsiorisoa.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 11:29:33'),
(118, '', 'ROMANINA', 'Nicolas', '', '', NULL, '', 0, '', '', '', 'S2-0119', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:46:16'),
(119, '', 'Roméo ', 'Ericka ', '', '', NULL, '', 0, '', '', '', 'S2-0120', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:46:10'),
(120, '', 'Safidiniriana', '  Anjarasoa Stellina', '0342388892', 'anjarasoa77@gmail.com', NULL, '', 1, '', 'informatique', '', 'S2-0121', 'B', '', 'stelina.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 10:08:41'),
(121, '', 'SOAZAFINIRINA ', 'Craussita Caromaine', '', '', NULL, '', 0, '', '', '', 'S2-0122', 'A', '', 'Craussita.jpg', NULL, 1, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 11:21:39'),
(122, '', 'TAFITANIAINA', 'Joel Israela', '', '', NULL, '', 0, '', '', '', 'S2-0123', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-12 16:50:45', '2021-03-13 06:44:44'),
(123, '', 'TANTELISAROBIDY', 'Nambinintsoa ', '0322710769', '', NULL, 'Andriambilany', 0, 'seconde', '', 'LTB alarobia', 'S2-0124', 'A', '', 'Tantely.jpg', NULL, 1, NULL, 'M', '2021-03-12 16:50:45', '2021-03-20 10:23:47'),
(124, '', 'tojofitiavana', 'Honitriniaina', '', '', NULL, '', 0, '', '', '', 'S2-0125', 'A', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:44:29'),
(125, '', 'TSILAVINA', 'Santatra Manampy Ny Aina ', '', '', NULL, '', 0, '', '', '', 'S2-0126', 'B', '', NULL, NULL, NULL, NULL, 'A', '2021-03-12 16:50:45', '2021-03-13 06:44:17'),
(126, NULL, 'RAJAONARIVONY', 'Koloina', '0340279190', 'koloina.arilala@gmail.com', NULL, 'Ankadifotsy', 1, '1 ere annee', 'informatique', 'ITU', 'S2-0126', 'A', 'mahafinaritra ahy ny robotika', NULL, NULL, NULL, NULL, NULL, '2021-03-13 07:05:14', '2021-03-13 07:05:14'),
(127, NULL, 'RAMAROSON', 'Aina Mamitiana', '0341081425', 'mamitiana626@gmail.com', NULL, 'Toamasina', 0, 'M1', 'informatique', 'IS Info', 'S2-0127', 'A', 'Finiavana hianatra technologie vaovao', NULL, NULL, NULL, NULL, NULL, '2021-03-13 07:07:15', '2021-03-13 07:07:15'),
(128, '', 'RAKOTOARINIRINA', 'Manampisoa', '0343365906', '', NULL, 'Akarion', 0, 'Bacc', '', '', 'S2-0128', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-13 07:08:54', '2021-03-13 09:09:18'),
(129, '', 'ANDRIANONY', 'Anjara Tiana', '0326594785', 'anjaratianaandrianony@gmail.com', NULL, 'Ampandrana', 1, '3e annee', 'Electro-mecanique', 'St Michel', 'S2-0127', 'C', 'partage et communication au sein du club', NULL, NULL, NULL, NULL, 'M', '2021-03-13 07:12:25', '2021-03-19 19:36:54'),
(130, '', 'RAMAMINIRINA', 'Njato Miaranarivo', '0344384737', 'njaxmia@gmail.com', NULL, 'Antsirabe', 1, '3e annee', 'Telecommumications', 'ESPA', 'S2-0128', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-13 08:18:54', '2021-03-20 10:06:36'),
(131, 'mirantsoa rasaholison', 'RASAHOLISON', 'Mirantsoa', '0344549094', 'mirantsoa102@gmail.com', NULL, '67ha sud', 0, 'bacc', '', '', 'S2-0130', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-13 10:33:27', '2021-03-13 10:34:07'),
(132, 'Toavina Lalaina', 'RANDRIANJAFY ', 'Toavina lalaina', '0346647377', 'toavinatop@gmail.com', NULL, 'moramanga', 1, '1 ere annee', 'electronique', 'ESPA', 'S2-0131', 'A', '', 'IMG_20210320_162512.jpg', NULL, 1, NULL, 'M', '2021-03-13 10:35:56', '2021-03-21 10:06:33'),
(133, 'ranto tsiferana', 'RAKOTOHARINIVO', 'Ranto Tsiferana', '0341905783', 'rantsotsiferanarakotoharinivo@gmail.com', NULL, 'Vatomandry', 0, 'lycee', '', '', 'S2-0132', 'A', '', 'rantotsiferana.jpg', NULL, 1, NULL, 'M', '2021-03-13 10:40:39', '2021-03-13 11:27:23'),
(134, '', 'RAKOTO', 'Iavontsoa Stephanie', '', '', NULL, '', 0, '', '', '', '', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-03-20 09:25:21', '2021-07-01 14:00:43'),
(136, '', 'RAKOTONOMENJANAHARY', 'Ny Aina Safidy', '', '', NULL, '', 0, '', '', '', 'S1', 'B', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 09:29:36', '2021-07-16 18:24:26'),
(137, '', 'RAKOTONIRINA', 'Liantsoa', '0340275382', '', NULL, 'befelatanana', 1, '1 ere annee', 'informatique', 'ITU', 'S2-0133', 'A', '', 'Liantsoa.jpg', NULL, 1, NULL, 'M', '2021-03-20 10:11:48', '2021-07-16 18:10:28'),
(138, '', 'RAMIANDRISOA', 'Harilalaina Stephan', '', 'ramiandrisoaharilalainastephen@gmail.com', NULL, 'Befelatanana', 1, '1ere année', 'Gestion', 'Université d\'Antananarivo', 'S2-0143', 'A', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 10:13:13', '2021-07-16 18:19:03'),
(139, '', 'RANDRIANARISOA', 'Ravaka Ny Aina', '', '', NULL, '', 1, '1 ere annee', 'informatique', 'ITU', 'S2-0142', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-03-20 10:32:40', '2021-07-16 18:17:56'),
(140, '', 'RAVELOARISON', 'Antsa Tahiana', '', '', NULL, '', 1, '2 e annee', 'genie electronique', 'St Michel', 'S2-0134', 'C', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 11:49:20', '2021-03-20 11:49:20'),
(141, '', 'RAVELOMANANTSOA', 'Faneva', '', '', NULL, '', 1, '2 e annee', 'Electro-mecanique', 'St Michel', 'S1-0135', 'C', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 11:50:49', '2021-03-20 11:50:49'),
(142, '', 'ANJARATIANA', 'Vally', '', '', NULL, '', 1, '2 e annee', 'Electro-mecanique', 'St Michel', 'S2-0136', 'C', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 11:51:40', '2021-03-20 12:35:56'),
(143, '', 'RANDRIANAINA', 'Nick Orylien', '', '', NULL, '', 1, '3e annee', 'Reseaux et systemes', 'CNTEMAD', 'S2-0137', 'C', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 11:53:28', '2021-03-20 11:53:28'),
(144, '', 'RAMIANDRISOA', 'Ninie', '', '', NULL, '', 1, '3e annee', 'genie industrielle', 'ISPM', 'S2-0138', 'C', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 11:54:26', '2021-03-20 11:54:26'),
(145, '', 'ANDRIAMIRADO', 'Tommy Liantsoa', '', '', NULL, '', 1, '3e annee', 'genie electronique', 'LTP Alarobia', 'S2-0139', 'C', '', NULL, NULL, NULL, NULL, 'M', '2021-03-20 11:58:14', '2021-03-20 11:58:14'),
(146, '', 'RAKOTOMAMONJY', 'Rado Lalaina', '0339236651', 'test@mail.com', NULL, 'Tana', 1, 'None', 'None', 'None', 'S2-0140', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-06-19 09:12:37', '2021-06-19 09:14:24'),
(147, '', 'RIVONANDRASANA', 'Hasina Juniolah', '0347460809', 'hasinajuunii123@gmail.com', NULL, 'Antsirabe', 1, 'L1', 'Informartique', 'ITU', 'S2-0141', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-06-19 09:20:30', '2021-06-19 09:20:30'),
(148, '', 'ROBISON ', 'Mirantotiana Fabuo ', '', '', NULL, '', 0, '', '', '', 'S2-0144', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-07-01 13:54:54', '2021-07-16 18:21:20'),
(149, '', 'RAZAFIARISOA  ', 'Arsène ', '', '', NULL, '', 0, '', '', '', 'S2-0145', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-07-01 13:59:38', '2021-07-16 18:22:04'),
(150, '', 'RAKOTOVELO ', 'Andriaminosoa Hary Joël ', '', '', NULL, '', NULL, '', '', '', '', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-07-01 14:02:02', '2021-07-01 14:02:38'),
(151, '', 'ANDIAMANALINA ', 'Rindra Finaritra ', '', '', NULL, '', NULL, '', '', '', '', 'C', '', NULL, NULL, 1, NULL, 'M', '2021-07-01 14:10:20', '2021-07-01 14:11:10'),
(152, '', 'RANDRIAMISAINA ', 'Tsiory ', '', '', NULL, '', NULL, '', '', '', '', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-07-01 14:12:22', '2021-07-01 14:12:51'),
(153, '', 'RANDRIAMBININTSOA ', 'Mandaniaina ', '', '', NULL, '', 0, '', '', '', '', 'C', '', NULL, NULL, 1, NULL, 'M', '2021-07-01 14:13:56', '2021-07-01 14:15:57'),
(154, '', 'ANDRIAMANALINA ', 'Mahery Nandrianina ', '', '', NULL, '', 0, '', '', '', 'S2-0146', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-07-01 14:23:06', '2021-07-16 18:22:46'),
(155, '', 'RABARISON ', 'Herijaona ', '', '', NULL, '', 0, '', '', '', 'S2-0147', 'A', '', 'IMG_PITU_20210627_113632.jpg', NULL, 1, NULL, 'A', '2021-07-01 15:29:51', '2021-07-16 18:23:41'),
(156, '', 'RABEANTOANDRO', 'Mirantsoa Andrianna', '0337487364', 'mirantsoarabeantoandro@gmail.com', NULL, 'Avaradoha', 1, 'ere anné', 'Electronique systeme informatique et intelligence artificielle', 'ISPM', 'S2-0148', 'A', '', NULL, NULL, 1, NULL, 'A', '2021-07-03 07:52:54', '2021-07-16 18:25:04'),
(157, '', 'RANDRIAMALALA', 'Antsatiana Princia', '', '', NULL, '', 0, '', '', '', 'S2-0149', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-07-03 09:16:25', '2021-07-16 18:25:37'),
(158, '', 'RAZAFIMBAHINY', 'Anjara Nasolo Erika', '', '', NULL, '', NULL, '', '', '', '', 'B', '', NULL, NULL, NULL, NULL, 'M', '2021-07-03 09:17:06', '2021-07-03 09:17:06'),
(159, '', 'SINEL', 'Vinesh', '', '', NULL, '', 0, '', '', '', 'S2-0150', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-07-03 09:17:55', '2021-07-16 18:26:22'),
(160, '', 'ANDRIATAHIANA', 'Vatosoa Finaritra', '', '', NULL, '', 0, '', '', '', 'S2-0151', 'A', '', NULL, NULL, 1, NULL, 'M', '2021-07-03 09:18:25', '2021-07-16 18:27:10'),
(161, '', 'RAKOTONINDRAINA', 'Steven Ritchie', '', '', NULL, '', 0, '', '', '', 'S2-0152', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-07-03 09:19:04', '2021-07-16 18:30:36'),
(162, '', 'ANDRIAMAMONJY', 'Johanesa Tiavina ', '', '', NULL, '', 0, '', '', '', 'S2-0153', 'B', '', NULL, NULL, 1, NULL, 'M', '2021-07-03 09:19:43', '2021-07-16 18:32:27');

-- --------------------------------------------------------

--
-- Structure de la table `Transcriptions`
--

CREATE TABLE `Transcriptions` (
  `id` int(11) NOT NULL,
  `bareme` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Transcriptions`
--

INSERT INTO `Transcriptions` (`id`, `bareme`, `title`, `createdAt`, `updatedAt`, `category`) VALUES
(4, '10', 'ALGO 1', '2021-03-19 20:09:47', '2021-03-19 20:09:47', NULL),
(5, '20', 'RESISTANCE', '2021-03-19 20:10:40', '2021-03-21 10:17:28', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `TStudents`
--

CREATE TABLE `TStudents` (
  `note` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `StudentId` int(11) NOT NULL,
  `TranscriptionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `TStudents`
--

INSERT INTO `TStudents` (`note`, `createdAt`, `updatedAt`, `StudentId`, `TranscriptionId`) VALUES
('16', '2021-03-21 10:19:11', '2021-03-21 10:19:11', 19, 5),
('20', '2021-03-21 10:18:44', '2021-03-21 10:18:44', 44, 5),
('19', '2021-03-21 10:14:02', '2021-03-21 10:14:02', 54, 5),
('17', '2021-03-21 10:17:16', '2021-03-21 10:17:16', 92, 5),
('19', '2021-03-21 10:18:20', '2021-03-21 10:18:20', 105, 5),
('17', '2021-03-21 10:17:56', '2021-03-21 10:17:56', 108, 5),
('13', '2021-03-21 10:16:44', '2021-03-21 10:16:44', 117, 5),
('10', '2021-03-21 10:12:06', '2021-03-21 10:12:06', 121, 5),
('17.5', '2021-03-21 10:13:01', '2021-03-21 10:13:01', 131, 5),
('19.5', '2021-03-21 10:10:48', '2021-03-21 10:10:48', 132, 5),
('13', '2021-03-21 10:11:25', '2021-03-21 10:11:25', 133, 5);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Coaches`
--
ALTER TABLE `Coaches`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Cprograms`
--
ALTER TABLE `Cprograms`
  ADD PRIMARY KEY (`programid`,`coachid`),
  ADD UNIQUE KEY `Cprograms_programid_coachid_unique` (`programid`,`coachid`),
  ADD KEY `coachid` (`coachid`);

--
-- Index pour la table `Events`
--
ALTER TABLE `Events`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Exercices`
--
ALTER TABLE `Exercices`
  ADD PRIMARY KEY (`programid`),
  ADD KEY `exerciceid` (`exerciceid`);

--
-- Index pour la table `Fees`
--
ALTER TABLE `Fees`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Files`
--
ALTER TABLE `Files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file` (`file`);

--
-- Index pour la table `FStudents`
--
ALTER TABLE `FStudents`
  ADD PRIMARY KEY (`FeeId`,`StudentId`),
  ADD KEY `StudentId` (`StudentId`);

--
-- Index pour la table `Notifications`
--
ALTER TABLE `Notifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `PFiles`
--
ALTER TABLE `PFiles`
  ADD PRIMARY KEY (`programid`,`FileId`),
  ADD KEY `FileId` (`FileId`);

--
-- Index pour la table `Programs`
--
ALTER TABLE `Programs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `PStudents`
--
ALTER TABLE `PStudents`
  ADD PRIMARY KEY (`programid`,`studentid`),
  ADD KEY `studentid` (`studentid`);

--
-- Index pour la table `SExercices`
--
ALTER TABLE `SExercices`
  ADD PRIMARY KEY (`exerciceid`,`studentid`),
  ADD UNIQUE KEY `SExercices_studentid_ExerciceProgramid_unique` (`studentid`,`ExerciceProgramid`),
  ADD KEY `ExerciceProgramid` (`ExerciceProgramid`);

--
-- Index pour la table `Students`
--
ALTER TABLE `Students`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `Transcriptions`
--
ALTER TABLE `Transcriptions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `TStudents`
--
ALTER TABLE `TStudents`
  ADD PRIMARY KEY (`StudentId`,`TranscriptionId`),
  ADD KEY `TranscriptionId` (`TranscriptionId`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Coaches`
--
ALTER TABLE `Coaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `Events`
--
ALTER TABLE `Events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `Fees`
--
ALTER TABLE `Fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Files`
--
ALTER TABLE `Files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `Notifications`
--
ALTER TABLE `Notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `Programs`
--
ALTER TABLE `Programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `Students`
--
ALTER TABLE `Students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT pour la table `Transcriptions`
--
ALTER TABLE `Transcriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Cprograms`
--
ALTER TABLE `Cprograms`
  ADD CONSTRAINT `Cprograms_ibfk_1` FOREIGN KEY (`programid`) REFERENCES `Programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Cprograms_ibfk_2` FOREIGN KEY (`coachid`) REFERENCES `Coaches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Exercices`
--
ALTER TABLE `Exercices`
  ADD CONSTRAINT `Exercices_ibfk_1` FOREIGN KEY (`exerciceid`) REFERENCES `Programs` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `Exercices_ibfk_2` FOREIGN KEY (`exerciceid`) REFERENCES `Students` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `Files`
--
ALTER TABLE `Files`
  ADD CONSTRAINT `Files_ibfk_1` FOREIGN KEY (`file`) REFERENCES `Programs` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `FStudents`
--
ALTER TABLE `FStudents`
  ADD CONSTRAINT `FStudents_ibfk_1` FOREIGN KEY (`FeeId`) REFERENCES `Fees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FStudents_ibfk_2` FOREIGN KEY (`StudentId`) REFERENCES `Students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `PFiles`
--
ALTER TABLE `PFiles`
  ADD CONSTRAINT `PFiles_ibfk_1` FOREIGN KEY (`programid`) REFERENCES `Programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `PFiles_ibfk_2` FOREIGN KEY (`FileId`) REFERENCES `Files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `PStudents`
--
ALTER TABLE `PStudents`
  ADD CONSTRAINT `PStudents_ibfk_1` FOREIGN KEY (`programid`) REFERENCES `Programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `PStudents_ibfk_2` FOREIGN KEY (`studentid`) REFERENCES `Students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `SExercices`
--
ALTER TABLE `SExercices`
  ADD CONSTRAINT `SExercices_ibfk_1` FOREIGN KEY (`studentid`) REFERENCES `Students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SExercices_ibfk_2` FOREIGN KEY (`ExerciceProgramid`) REFERENCES `Exercices` (`programid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `TStudents`
--
ALTER TABLE `TStudents`
  ADD CONSTRAINT `TStudents_ibfk_1` FOREIGN KEY (`StudentId`) REFERENCES `Students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `TStudents_ibfk_2` FOREIGN KEY (`TranscriptionId`) REFERENCES `Transcriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
